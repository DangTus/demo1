#include<stdio.h>
struct Nut
{
	int dl;
	Nut *tr, *ph;
};

Nut *BSung(Nut *cay, Nut *ptu) {
	Nut *tg, *trc;
	if(cay==NULL) {
		cay=ptu;
	}
	else {
		tg=cay;
		while(tg!=NULL) {
			trc=tg;
			if(tg->dl > ptu->dl)
				tg=tg->tr;
			else
				tg=tg->ph;
		}
		if(trc->dl > ptu->dl)
			trc->tr= ptu;
		else
			trc->ph= ptu;		
	}
	return cay;
}

Nut *Nhap(Nut *cay, int pt) {
	Nut *ptu;
	int i;
	for(i=1; i<=pt; i++) {
		ptu= new Nut;
		scanf("%d", &ptu->dl);
		ptu->tr= NULL;
		ptu->ph= NULL;
		cay= BSung(cay, ptu);
	}
	return cay;
}

void In(Nut *cay) {
	if(cay!=NULL) {		
		In(cay->tr);		
		printf("%d  ", cay->dl);
		In(cay->ph);		
	}
}

Nut *TimKiem(Nut *cay, int x) {
	Nut *tg;
	tg=cay;
	while(tg!=NULL && tg->dl!=x) {
		if(tg->dl > x)
			tg = tg->tr;
		else
			tg= tg->ph;
	}
	if(tg!=NULL)
		return tg;
	else
		return NULL;
}

Nut *xoa(Nut *cay, int x) {
	Nut *tg, *phcung, *trc;
	while(TimKiem(cay, x) != NULL) 
	{
		tg=TimKiem(cay, x);
		if(tg->tr!=NULL && tg->ph!=NULL)
		{
			if(tg==cay)
				cay=tg->tr;
			else 
			{
				trc=cay;
				while(trc->tr!=tg && trc->ph!=tg) {
					if(trc->dl > x)
						trc=trc->tr;
					else
						trc=trc->ph;
				}				
				if(trc->tr == tg)
					trc->tr= tg->tr;
				else
					trc->ph= tg->tr;
			}
			phcung=tg->tr;
			while(phcung->ph!=NULL)
				phcung=phcung->ph;
			phcung->ph=tg->ph;
			tg->tr=NULL;
			tg->ph=NULL;
			delete tg;
		}
		else
			if(tg->tr!=NULL && tg->ph==NULL)
			{
				if(tg==cay)
					cay=tg->tr;
				else
				{
					trc=cay;
					while(trc->tr!=tg && trc->ph!=tg) {
						if(trc->dl > x)
							trc=trc->tr;
						else
							trc=trc->ph;
					}				
					if(trc->tr == tg)
						trc->tr= tg->tr;
					else
						trc->ph= tg->tr;
				}
				tg->tr=NULL;
				delete tg;
			}
			else
				if(tg->tr==NULL && tg->ph!=NULL)
				{
					if(tg==cay)
						cay=tg->ph;
					else
					{
						trc=cay;
						while(trc->tr!=tg && trc->ph!=tg) {
							if(trc->dl > x)
								trc= trc->tr;
							else
								trc=trc->ph;
						}
						if(trc->tr == tg)
							trc->tr= tg->ph;
						else
							trc->ph= tg->ph;
					}
					tg->ph=NULL;
					delete tg;
				}
				else
				{
					if(tg==cay)
						cay=NULL;
					else
					{
						trc=cay;
						while(trc->tr!=tg && trc->ph!=tg) {
							if(trc->dl > x)
								trc= trc->tr;
							else
								trc=trc->ph;
						}
						if(trc->tr == tg)
							trc->tr= NULL;
						else
							trc->ph= NULL;
					}
					delete tg;
				}
	}
	return cay;
}

main()
{
	Nut *c;
	int n, so;
	c=NULL;
	printf("Tra loi, ban muon nhap bao nhieu so? ");
	scanf("%d", &n);
	c= Nhap(c, n);
	In(c);
	printf("\nNhap so can tim kiem: ");
	scanf("%d", &so);
	if(TimKiem(c, so) != NULL)
		printf("co");
	else
		printf("khong");
	printf("\nNhap so can xoa: ");
	scanf("%d", &so);
	c= xoa(c, so);
	In(c);
}
